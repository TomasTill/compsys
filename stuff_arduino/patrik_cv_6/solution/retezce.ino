#include "input.h"
#include "funshield.h"
#include <ctype.h>

SerialInputHandler input;

constexpr byte LETTER_GLYPH[] {
      0b10001000,   // A
      0b10000011,   // b
      0b11000110,   // C
      0b10100001,   // d
      0b10000110,   // E
      0b10001110,   // F
      0b10000010,   // G
      0b10001001,   // H
      0b11111001,   // I
      0b11100001,   // J
      0b10000101,   // K
      0b11000111,   // L
      0b11001000,   // M
      0b10101011,   // n
      0b10100011,   // o
      0b10001100,   // P
      0b10011000,   // q
      0b10101111,   // r
      0b10010010,   // S
      0b10000111,   // t
      0b11000001,   // U
      0b11100011,   // v
      0b10000001,   // W
      0b10110110,   // ksi
      0b10010001,   // Y
      0b10100100,   // Z
    };

class Repeater {
  public:
    bool hasIntervalPassed(const long int interval) {
      long int currTime = millis();
      if (currTime >= _lastTime + interval) {
        _lastTime = currTime;
        return true;
      }
      return false;
    }
  private:
    long int _lastTime = 0;
};

class Debug {
  public:
  void out(const char* str)
  {
//      Serial.print("> ");
//      Serial.print(str);
//      Serial.print("\n");
  }
  void varValue(const char* varName, const int value)
  {
//      Serial.print(varName);
//      Serial.print(": ");
//      Serial.print(value);
//      Serial.print("\n");
  }
  private:
};

Debug d;

int delkaRetezce(const char* str) {
  int i = 0;
  // implicitne uzavorkovano *(str++)
  while (*str++) ++i;
  return i;
}

class SegmentDisplay {
  public:

  /**
   * set text which has length smaller than displayCount
   */
  void setShortText(const char* str) {
    int delka = delkaRetezce(str);
    const int maxDelka = displayCount;
    for (int i = 0;i < maxDelka;++i) {
      if (i < delka)
      {
        setCharL(*str, i);
        str++;
      } else {
        setCharL(' ', i);
      }
      
    }
  }

  /**
   * @param index - index position of display from the right
   */
  void setChar(const char c, const int index) {
    byte glyph = EMPTY_GLYPH;
    if (isalpha(c))
      glyph = LETTER_GLYPH[c - (isUpperCase(c) ? 'A' : 'a')];
    currGlyphs[index] = glyph;
  }

  /**
   * @param index from the left
   */
  void setCharL(char c, int index) {
    setChar(c, displayCount - 1 - index);
  }

  void loop(){
    writeGlyphR(currGlyphs[pos_], pos_);
    pos_ = (pos_ + 1) % displayCount; 
  }
  
  void writeGlyph(byte glyph, byte posBitMask) {
    digitalWrite(latch_pin, LOW);
    shiftOut(data_pin, clock_pin, MSBFIRST, glyph);
    shiftOut(data_pin, clock_pin, MSBFIRST, posBitMask);
    digitalWrite(latch_pin, HIGH);
  }
  
   void writeGlyphL(byte glyph, int posFromLeft) {
    writeGlyph(glyph, 1 << posFromLeft);
   }

   void writeGlyphR(byte glyph, int posFromRight) {
    writeGlyph(glyph, 0b1000 >> posFromRight);
   }


   /**
    * turns off all lights and resets internal state
    */
   void makeEmpty() {
       for (int i = 0; i < displayCount;++i) {
        currGlyphs[i] = EMPTY_GLYPH;
       }
   }

   /**
    * displays long text once
    */
   void setLongText(const char* str) {
      nextChar = (char*)str;
   }

   /**
    * return true if cycle is finished
    */
   bool moveText() {
    // shift letters 
    for (int i = displayCount - 2; i >= 0;--i) {
      currGlyphs[i + 1] = currGlyphs[i];
//      d.varValue("i", i);
    }
    if (*nextChar)
    {
      setChar(*nextChar, 0);
      ++nextChar;
    }
    else
    {
      if (countAddedSpaces < displayCount - 1)
      {
        setChar(' ', 0);
        ++countAddedSpaces;
      }
      else
      {
        setChar(' ', 0);
        countAddedSpaces = 0;
        return true;
      }
    }
    return false;
   }
   
  private:
    int pos_ = 0; // pos in loop
    // index 0 is the left right one
    int currGlyphs[4] = { 0, 0, 0, 0 };
    int displayCount = sizeof(currGlyphs) / sizeof(currGlyphs[0]);
    char* nextChar;
    byte EMPTY_GLYPH = 0b11111111;
    int countAddedSpaces = 0;
    
};


SegmentDisplay segDis;
Repeater rep;

/**
 * requires segDis.moveText() in the loop
 */
void x61_setChar(const char c, int index) {
  segDis.setChar(c, index);
}

/**
 * requires segDis.moveText() in the loop
 */
void x62_setShortText(const char* c) {
  segDis.setShortText(c);
}

/**
 * requires segDis.moveText() in the loop
 */
void x63_setRunningText(const char* c) {
  segDis.setLongText(c);
}

void setup() {
  // put your setup code here, to run once:
//  Serial.begin(9600);
  pinMode(latch_pin, OUTPUT);
  pinMode(clock_pin, OUTPUT);
  pinMode(data_pin, OUTPUT);
  segDis.makeEmpty();
  input.initialize();
//  x61_setChar('c', 1);
//  x62_setShortText("lala");
//  x63_setRunningText("hello world");
}

bool isCycleFinished = true;

void loop() {
  // 6.1
//  segDis.loop();
  // 6.2
//  segDis.loop();
  // 6.3
//  if (rep.hasIntervalPassed(300))
//    segDis.moveText();
  // 6.5
  input.updateInLoop();
  
  if (rep.hasIntervalPassed(300))
  {
    if (isCycleFinished)
    {
      const char* newMessage = input.getMessage();
      segDis.setLongText(newMessage);
    }
    isCycleFinished = segDis.moveText();
  }
  segDis.loop();
  
}
