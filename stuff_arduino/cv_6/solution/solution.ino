#include "funshield.h"
#include "input.h"
#include <string.h>

class Button{
  public:
    int pin;

    bool isPressed(int interval = 0, bool measure_after_still_pressed = false){
      return internal_button_delay(interval, measure_after_still_pressed);
    }

  Button(int pin_){
    pin = pin_;
  }
  
  private:
    unsigned long last_time = 0;
    bool last_state = false;                // used in button_delay
    bool is_measured = false;               // used in button delay

    bool button_pressed(){
      return digitalRead(pin) ? false : true;
    }   

    bool internal_button_delay(int interval, bool measure_after_still_pressed = false){
      bool cur_state = button_pressed();
      bool is_pressed = cur_state;                           // to better understand the code
      if (cur_state != last_state){                          // has the state of the button changed?
        if (is_pressed){
          last_time = millis();                              // if yes and the button is pressed, starts measuring from the current time
          is_measured = true;
        }
        else{
          is_measured = false;                               // if the state has changed but the button is not pressed -> from pressed to unpressed -> stops measuring
        }
        last_state = cur_state;                              // updates the state of the button
      }   
      if (is_measured){                                      // button is measured only if it's pressed
        if (is_pressed){
          if (millis() >= (last_time + interval)){
            if (measure_after_still_pressed){
              last_time = millis();                          // doesn't change is_measured to false, changes last time to current time -> resets the button -> function returns true every time the interval elapses
            }
            else{
              is_measured = false;                           // after the interval elapses, is_measured = false -> returns true only once
            }
            return true;
          }
        }
      }   
      return false;
  }
};

Button button[]{button1_pin, button2_pin, button3_pin}; 
constexpr int button_count = sizeof(button)/sizeof(button[0]);
SerialInputHandler input;

class Display
{
public:
  void displayChar(const char* ch, int pos){
    byte g = getGlyph(*ch);
    writeGlyph(g, pos);
  }
  void displayTextFixedSize(const char *text){
    byte g;
    int pos = displayCount - strlen(text);
    while (*text)
    {
      g = getGlyph(*text++);
      glyphsToShow[pos++] = g;
    }
  }

  void setShowNumber (int showNumber){
      // z n vytvori pole glyfu
      // cislo rozstrskat na cifry, ty prelozit na glyph a dat do pole
      constexpr int glyph_number[10] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90};

      for (int i = displayCount - 1; 0 <= i; --i){
        int cifra = get_cifra(showNumber, i);                            //beru si ty cifra v poradi od tisicu po jednotky 
        glyphsToShow[(displayCount - 1) - i] = glyph_number[cifra];        //ukladam si je i v tomto poradi (na pozici 0 jsou ticise, na pozici 1 jsou stovky ...)
      }
      removeLeadingZeroes();
    }

  void displayTextDynamicSize(const char* text, int speedOfChange, bool toRight = true){
    const int trailingSpacesTotalCount = 4; const char firstLetter = *text;

    if (isFinishedShowing){
      // resets the pointer to the beggining of the text
      if (toRight){
        currLetterInTextPtr = text;                   
      }
      else{
        const char* tempPtr = text;
        while(*tempPtr) tempPtr++; tempPtr--;               // tempPtr is showing on the last letter of text
        currLetterInTextPtr = tempPtr;                   
      }

      for (int i = 0; i < displayCount; i++){
        glyphsToShow[i] = noGlyph;                  // reset to spaces
      }

      toRight ? updateText(*currLetterInTextPtr++, toRight) : updateText(*currLetterInTextPtr--, toRight);           // in order to show the first letter already
      trailingSpacesWrittenCount = 0;
      isFinishedShowing = false;
      displayDirection = toRight;

    }
    else{
      if(loopDelay(speedOfChange)){
        if(*currLetterInTextPtr){
          if (displayDirection){
            updateText(*currLetterInTextPtr++, displayDirection);
          }
          else{
            updateText(*currLetterInTextPtr--, displayDirection);
            if (*currLetterInTextPtr == firstLetter){
              updateText(*currLetterInTextPtr, displayDirection);                 // in order to show the first letter (last letter to show)
              while(*currLetterInTextPtr++); currLetterInTextPtr--;               // force currLetter to show to null byte
            }
          }
        }
        else{
          if (trailingSpacesWrittenCount != trailingSpacesTotalCount){
            updateText(noGlyph, displayDirection);
            trailingSpacesWrittenCount += 1;
          }
          else{
            isFinishedShowing = true;
          }
        }
      }
    }
  }

  void showMessageFromSerialMonitor(int speedOfChange){
    if (isFinishedShowing){
      const char* newText = input.getMessage();
      textToShow = newText;
    }
    displayTextDynamicSize(textToShow, speedOfChange);
  }

  void interactiveMovingText(const char* text, int speedOfChange){
    const int speedChangeInterval = 50;

    //init
    if (dynamicSpeedOfChange == 0){
      dynamicSpeedOfChange = speedOfChange;
      buttonDirection = true;
    }

    if (button[0].isPressed()){
      if (dynamicSpeedOfChange > 0) dynamicSpeedOfChange -= speedChangeInterval;
    }

    if (button[1].isPressed()){
      buttonDirection ? buttonDirection = false : buttonDirection = true;
    }

    if (button[2].isPressed()){
      dynamicSpeedOfChange += speedChangeInterval;
    }
    displayTextDynamicSize(text,dynamicSpeedOfChange, buttonDirection);
  }

  void loop(){
    // zobrazi pole glyfu na displej
    writeGlyph(glyphsToShow[(displayCount - 1) - positionCifra], positionCifra); //v glypshToShow je cislo ulozene zleva doprava -> musime obratit pozice
    positionCifra = (positionCifra + 1) % displayCount;
  }

  void initialize(){
    pinMode(latch_pin, OUTPUT);
    pinMode(data_pin, OUTPUT);
    pinMode(clock_pin, OUTPUT);
  }

  Display() {}

private:
  const int displayCount = 4;
  int positionCifra = 0;                                                    // used in multiplexing - loop function
  const int zero = 0xC0; const int dot = 0x7F; const int noGlyph = 0xFF;
  int glyphsToShow[4] = {noGlyph, noGlyph, noGlyph, noGlyph};               // what is written to this array is shown on the display

  bool isFinishedShowing = true; const char* currLetterInTextPtr; int trailingSpacesWrittenCount = 0;           // used in 6.5
  unsigned long lastTime = 0;                                                                                   // used in 6.5 - for loop delay 
  const char* textToShow = "Hello World";                                                                       // this text is actually never shown - had to be intitialized just for the compiler
  unsigned long dynamicSpeedOfChange = 0; bool buttonDirection; bool displayDirection;

  bool loopDelay(int interval){
    auto curTime = millis();
    if (curTime >= lastTime + interval){
      lastTime = curTime;
      return true;
    }
    return false;
  }

  void updateText(char currLetter, bool toRight = true){
    const int lastLetterPosition = 3; const int firstLetterPosition = 0;
    int newLetterPosition;
    byte glyph = getGlyph(currLetter);

    // moves text
    if (toRight){
      // moves it to right
      for (int i = 0; i < displayCount - 1; i++){
        glyphsToShow[i] = glyphsToShow[i + 1];
        newLetterPosition = lastLetterPosition;
      }
    }
    else{
      // moves it to left
      for (int i = displayCount - 1; i > 0; i--){
        glyphsToShow[i] = glyphsToShow[i - 1];
        newLetterPosition = firstLetterPosition;
      }
    }

    // udpates the new letter with currLetter
    glyphsToShow[newLetterPosition] = glyph;
  }

  byte getGlyph(char ch){
    constexpr byte empty_glyph = 0b11111111;
    constexpr byte glyph_number[10] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90};
    constexpr byte letter_glyph[] {
          0b10001000,   // A
          0b10000011,   // b
          0b11000110,   // C
          0b10100001,   // d
          0b10000110,   // E
          0b10001110,   // F
          0b10000010,   // G
          0b10001001,   // H
          0b11111001,   // I
          0b11100001,   // J
          0b10000101,   // K
          0b11000111,   // L
          0b11001000,   // M
          0b10101011,   // n
          0b10100011,   // o
          0b10001100,   // P
          0b10011000,   // q
          0b10101111,   // r
          0b10010010,   // S
          0b10000111,   // t
          0b11000001,   // U
          0b11100011,   // v
          0b10000001,   // W
          0b10110110,   // ksi
          0b10010001,   // Y
          0b10100100,   // Z
        };

    byte glyph = empty_glyph;
    if (isAlpha(ch))
    {
      glyph = letter_glyph[ch - (isUpperCase(ch) ? 'A' : 'a')];
    }
    else if (isAlphaNumeric(ch))
    {
      glyph = glyph_number[ch - '0'];
    }
    else if (isdigit(ch))
    {
      glyph = glyph_number[ch];
    }
    return glyph;
  }

  void writeGlyph(byte glyph_mask, byte position){
    constexpr int bitmask_position[] = {1 << 3, 1 << 2, 1 << 1, 1 << 0};
    digitalWrite(latch_pin, LOW);
    shiftOut(data_pin, clock_pin, MSBFIRST, glyph_mask);
    shiftOut(data_pin, clock_pin, MSBFIRST, bitmask_position[position]);
    digitalWrite(latch_pin, HIGH);
  }
};
Display display;

void setup(){
  input.initialize();
  display.initialize();
}

const int userInterval = 300;
void loop(){
  //display.displayChar("P", 0);                                              // 6.1
  //display.displayTextFixedSize("ahoj");                                     // 6.2 
  //display.displayTextDynamicSize("ARDUINO",userInterval, true);             // 6.3
  //display.interactiveMovingText("ARDUINO",userInterval);                    // 6.4
  display.showMessageFromSerialMonitor(userInterval);                        // 6.5

  input.updateInLoop();
  display.loop();
}
