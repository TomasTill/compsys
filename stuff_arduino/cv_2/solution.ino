#include "funshield.h"

// variables setup
constexpr int led[]{led1_pin, led2_pin, led3_pin, led4_pin};  // array of leds
constexpr int led_count = sizeof(led)/sizeof(led[0]);

unsigned long last_time[]{0,0,0,0,0};                         // index 0-3 led times, index 4 external time
bool light_on[]{true, true, true, true};                      // array of light on/off state of leds

int n = 0;                      // used in count_up, increases by 1 to inf
int led_i = 0, led_smer = 1;    // used in behajici_kulicka


// functions
// turns a led on/off
void control_led(int i, bool light_on){
digitalWrite(led[i], light_on ? ON : OFF);
}

// non-blocking delay
bool loop_delay(int i, int interval){
  unsigned long cur_time = millis();
  if (cur_time >= last_time[i] + interval){
    last_time[i] = cur_time;
    return true;
  }
  return false;
}

// blicks a led for an interval
void x2_1_blik_led(int i, int interval){
  if (loop_delay(i, interval)){
    control_led(i,light_on[i]);
    light_on[i] = !(light_on[i]);
  }
}

// blicks two leds // used in semafor
void blik_two_leds(int i, int j, int interval){
  x2_1_blik_led(i, interval);
  x2_1_blik_led(j, interval);
}

// blicks two leds and then the other two leds in a specified interval
void x2_2_semafor(int interval){
  if(light_on[2] && light_on[3]){
    blik_two_leds(0,1,interval);
  }
  if(light_on[0] && light_on[1]){
    blik_two_leds(2,3,interval);
  }
}

// lights off all leds
void light_off_leds(){
  for(int i = 0; i < led_count; ++i) control_led(i, false);
}

// used in count_up
void binary_decomposition(int n, int interval){
  for (int k = 0; k < led_count; ++k){
    bool k_bit = n & (1 << k);
    if (k_bit){
      control_led(k, true);
    }
  }
}

// counts up to infinity and displays last four bits on leds
void x2_3_count_up(int interval){
  if (loop_delay(4, interval)){
      light_off_leds();
      n = n + 1;
      binary_decomposition(n, interval);
  }
}

// lights up a led and changes the global index // used in behajici kulicka
void kulicka_pohyb(){
  control_led(led_i, true);
  led_i = led_i + led_smer;
}

// makes an impression of a bouncing/running ball
void x2_4_behajici_kulicka(int interval, bool verze = 0){
  if (loop_delay(4,interval)){
    light_off_leds();
    if (verze){
      led_smer = 1;
      if (led_i > 3) led_i = 0;
    }
    else{
      if (led_i == 0){
        led_smer = 1;
      }
      if (led_i == 3){
        led_smer = -1; 
      }  
    }
    kulicka_pohyb();
  }
}

// initialization
void setup() {
  for (int i = 0; i < led_count; ++i){
    pinMode(led[i], OUTPUT);
    digitalWrite(led[i], OFF);
  }
}

// main
void loop() {
  //control_led(3, true);
  //x2_1_blik_led(0,300);
  //x2_2_semafor(700);
  //x2_3_count_up(1000);
  x2_4_behajici_kulicka(300);
  //x2_4_behajici_kulicka(300, 1);   // 0/nic - odrazi se, 1 - dokola
  }
