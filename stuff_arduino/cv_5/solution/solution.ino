#include "funshield.h"


class Button{
  public:
    int pin;

    bool isPressed(int interval = 0, bool measure_after_still_pressed = false){
      return internal_button_delay(interval, measure_after_still_pressed);
    }

  Button(int pin_){
    pin = pin_;
  }
  
  private:
    unsigned long last_time = 0;
    bool last_state = false;                // used in button_delay - very essantial
    bool is_measured = false;               // used in button delay - very essantial

    bool button_pressed(){
      return digitalRead(pin) ? false : true;
    }

    bool internal_button_delay(int interval, bool measure_after_still_pressed = false){
      bool cur_state = button_pressed();
      bool is_pressed = cur_state;                           // to better understand the code
      if (cur_state != last_state){                          // has the state of the button changed?
        if (is_pressed){
          last_time = millis();                              // if yes and the button is pressed, starts measuring from the current time
          is_measured = true;
        }
        else{
          is_measured = false;                               // if the state has changed but the button is not pressed -> from pressed to unpressed -> stops measuring
        }
        last_state = cur_state;                              // updates the state of the button
      }
      if (is_measured){                                      // button is measured only if it's pressed
        if (is_pressed){
          if (millis() >= (last_time + interval)){
            if (measure_after_still_pressed){
              last_time = millis();                          // doesn't change is_measured to false, changes last time to current time -> resets the button -> function returns true every time the interval elapses
            }
            else{
              is_measured = false;                           // after the interval elapses, is_measured = false -> returns true only once
            }
            return true;
          }
        }
      }
      return false;
  }
};

Button button[]{button1_pin, button2_pin, button3_pin}; 
constexpr int button_count = sizeof(button)/sizeof(button[0]);

class Display{
  public:
    int digitCount = 4;
    void setShowNumber (int showNumber){
      // z n vytvori pole glyfu
      // cislo rozstrskat na cifry, ty prelozit na glyph a dat do pole
      constexpr int glyph_number[10] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90};

      for (int i = digitCount - 1; 0 <= i; --i){
        int cifra = get_cifra(showNumber, i);                            //beru si ty cifra v poradi od tisicu po jednotky 
        glyphsToShow[(digitCount - 1) - i] = glyph_number[cifra];        //ukladam si je i v tomto poradi (na pozici 0 jsou ticise, na pozici 1 jsou stovky ...)
      }
      removeLeadingZeroes();
    }

    void setShowFloatNumber(unsigned long showFloatNumber, int dotPosition){
      //jenom zobrazuje desetinne cislo 
      //dot position se bere od nejnizsich jednotek, takze zprava cisla
      setShowNumber(showFloatNumber);
      if (0 < dotPosition && dotPosition < digitCount){
        glyphsToShow[(digitCount - 1) - dotPosition] = glyphsToShow[(digitCount - 1) - dotPosition] & dot;
      }
    }

    void setStopwatch(unsigned long number){
      // showNumber je s presnosti 1000ms (vzano z millis), ukaz na dispej s presnosti na destiny sekundy
      if (number < 0) return;             //ERROR!
      unsigned long showNumber = number / 100;   // deleni 100 -> z presnosti na tisicovky chci presnost na stovky (presnost zobrazovani je resena ve stopwatch podle recodexu - tady se jedna o jinou presnost)
      setShowFloatNumber(showNumber, 1);
      if (number < 1000) glyphsToShow[2] = zero & dot;
    }

    void loop(){
      // zobrazi pole glyfu na displej
      write_glyph_number(glyphsToShow[(digitCount - 1) - positionCifra], positionCifra); //v glypshToShow je cislo ulozene zleva doprava -> musime obratit pozice
      positionCifra = (positionCifra + 1) % digitCount;
    }

    void initialize(){
      pinMode(latch_pin, OUTPUT); pinMode(data_pin, OUTPUT); pinMode(clock_pin, OUTPUT);
      write_glyph_bitmask(0xFF,0x0F);
    }

    Display(){}
    
  private:
    int positionCifra = 0; 
    int glyphsToShow[4];
    int zero = 0xC0; int dot = 0x7F; int noNumber = 0xFF;

    void write_glyph_bitmask(int byte_glyph, int byte_pos_mask){
      digitalWrite(latch_pin, LOW);
      shiftOut(data_pin, clock_pin, MSBFIRST, byte_glyph);
      shiftOut(data_pin, clock_pin, MSBFIRST, byte_pos_mask);
      digitalWrite(latch_pin, HIGH);
    }

    void removeLeadingZeroes(){
      for (int i = 0; i < digitCount - 1; ++i) if (glyphsToShow[i] == 0xC0) glyphsToShow[i] = noNumber; else break;
    }

    int number_position(int position){
      switch (position){
        case 1: position = 8; break; case 2: position = 4; break; case 4: position = 2; break; case 8: position = 1; break;
      }
      return position;
    }

    void write_glyph_number(int glyph_mask, int position){
      // number = number, position = number between 0 - 3, on display from the right
      constexpr int bitmask_position[] = {1 << 0, 1 << 1, 1 << 2, 1 << 3};                // 0th position is the rightest one
      digitalWrite(latch_pin, LOW);
      shiftOut(data_pin, clock_pin, MSBFIRST, glyph_mask);
      shiftOut(data_pin, clock_pin, MSBFIRST, number_position(bitmask_position[position]));
      digitalWrite(latch_pin, HIGH);
    }

    int get_rad(int position){
      int increment;
      switch (position){
        case 0: increment = 1; break; case 1: increment = 10; break; case 2: increment = 100; break; case 3: increment = 1000; break;
      }
      return increment;
    }

    int get_cifra(int n, int cifra_pos){
      // cifra pos je zprava do leva - tedy 0 - jednotky, 1 - desitky ...
      int rad = get_rad(cifra_pos);
      int cifra = (n / rad) % 10;
      return cifra;
    }
};

class StopWatch{
  public:
    StopWatch(unsigned long time = 0){
      internalTime = time; 
      currentState = Stopped;
    }
    void Run(){
      inStatesControl();
    }
  
  private:
    unsigned long internalTime; unsigned long showTime = internalTime; unsigned long lastTime = internalTime; unsigned long ghostTime = 0;
    Button button1 = button[0]; Button button2 = button[1]; Button button3 = button[2];
    enum state {Stopped, Running, Lapped}; 
    enum state currentState;

    void timeReset(){
      //zobrazit na dispej nulu
      internalTime = 0; showTime = 0; lastTime = 0;
    }

    void timeStop(){
      //na displej ukazuj posledni namerenou hodnotu
      showTime = lastTime;
    }

    void timeStopMeasure(){
      ghostTime = millis() - lastTime;
    }

    void timeMeasure(){
     internalTime = millis() - ghostTime;
    }

    void timeShow(){
      //na displej ukazuj time
      extern Display display;
      display.setStopwatch(showTime);
    }

    void inStatesControl(){
      bool button1Pressed = button1.isPressed(); bool button2Pressed = button2.isPressed(); bool button3Pressed = button3.isPressed();
      switch (currentState){
        case Stopped:
          timeStop(); 
          timeStopMeasure();
          if (button1Pressed){
            currentState = Running;
          }

          if (button3Pressed){
            timeReset();
          }
          break;
        
        case Running:
          timeMeasure();
          showTime = internalTime;
          if(button1Pressed){
            lastTime = showTime;
            currentState = Stopped;
          }

          if(button2Pressed){
            lastTime = showTime;
            currentState = Lapped;
          }
          break;

        case Lapped:
          timeStop();     
          timeMeasure();
          if (button2Pressed){
            currentState = Running;
          }
          break;
      }
    timeShow();
    }

};

class Button_display_counter_managment_variables{
  public:
    int counter = 0;      
    int position = 0;     // position of the display

    int get_increment(int position){
      int increment;
      switch (position){
        case 0: increment = 1; break; case 1: increment = 10; break; case 2: increment = 100; break; case 3: increment = 1000; break;
      }
      return increment;
    }

    Button_display_counter_managment_variables(int counter_, int starting_position_){
      counter = counter_; position = starting_position_;
    }
};

//button initialization
Display display;
Button_display_counter_managment_variables m (0,0); 
StopWatch stopwatch;

// functions for exercises
void buttonDisplayCounterFullNumber(){
  int zero_seconds = 0;    //button_delay function needs to have a specified interval for how long to measure - more details in the function specifics
  int first_button = 0; int second_button = 1; int third_button = 2;
  int increment = m.get_increment(m.position); int modulo = 10000;          // current increment
  display.setShowNumber(m.counter);

  if (button[0].isPressed(zero_seconds)){
    m.counter = (m.counter + increment) % modulo;    // first button adds
  }
  if (button[1].isPressed(zero_seconds)){
    if ((m.counter - increment) < 0) m.counter = modulo - (increment - m.counter);
    else m.counter = (m.counter - increment);       // second button substracts

  }
  if (button[2].isPressed(zero_seconds)){
    m.position = (m.position + 1) % 4;              // there is four displays
  }
}

void setup() {
  // initializes buttonsjj
  for (int i = 0; i < button_count; ++i){
    pinMode(button[i].pin, INPUT);
  }
  //Serial.begin(9600);
  display.initialize();
  //display.setShowNumber(420);           //5.1
}

void loop() {
  //buttonDisplayCounterFullNumber();     //5.2
  stopwatch.Run();                      //5.3
  display.loop();
}