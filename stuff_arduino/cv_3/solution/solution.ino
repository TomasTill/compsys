#include "funshield.h"

//setup promennych
class Button{
  public:
    int pin;
    unsigned long last_time = 0;
    bool last_state = false;
    bool is_measured = false;

    bool first_access;        // used in x3_5
    bool immidiate_access;    // used in x3_5

    Button(int pin_){
      pin = pin_;
    }
};

class Led{
  public:
    int pin;
    unsigned long last_time = 0;
    bool light_on;
    bool is_flashing = false;
    
    Led(int pin_){
      pin = pin_;
    }
};

//ex. 3.3 and 3.5
class ButtonBinaryCountUp{
  public:
    int n; //used in 3.3 and 3.5
    ButtonBinaryCountUp(int n_){
       n = n_; 
    }
};
ButtonBinaryCountUp counter(0);


// Inicialization of leds and buttons
Led led1(led1_pin); Led led2(led2_pin); Led led3(led3_pin); Led led4(led4_pin);
Led led[]{led1, led2, led3, led4};
constexpr int led_count = sizeof(led)/sizeof(led[0]);

Button button1(button1_pin); Button button2 (button2_pin); Button button3 (button3_pin);
Button button[]{button1, button2, button3};
constexpr int button_count = sizeof(button)/sizeof(button[0]);


// helper functions
// simple loop delay. Named led_delay, because it's used only for leds. Not suitable for buttons 
bool led_delay(int led_i, int interval){
  unsigned long cur_time = millis();
  if (cur_time >=led[led_i].last_time+ interval){
    led[led_i].last_time = cur_time;
    return true;
  }
  return false;
}

bool button_pressed(int button_i){
  return digitalRead(button[button_i].pin) ? false : true;
}

// very essential function. Used in x3_2, x3_3, x3_4
// Has two states 
// 1. returns true after the given interval elapsed even if the button was not released and it's still held down-> default state -> true
// 2. returns true after the given interval elapsed only once. Does not return true if the button was not released -> false 
bool button_delay(int button_i, int interval, bool measure_after_still_pressed = true){
  bool cur_state = button_pressed(button_i);
  if (cur_state != button[button_i].last_state){                          // has the state of the button changed?
    if (button_pressed(button_i)){
      button[button_i].last_time = millis();                              // if yes and the button is pressed, starts measuring from the current time
      button[button_i].is_measured = true;
    }
    else{
      button[button_i].is_measured = false;                               // if the state has changed but the button is not pressed -> from pressed to unpressed -> stops measuring
    }
    button[button_i].last_state = cur_state;                              // updates the state of the button
  }

  if (button[button_i].is_measured){                                      // button is measured only if it's pressed
    if (button_pressed(button_i)){
      if (millis() >= (button[button_i].last_time + interval)){
        if (measure_after_still_pressed){
          button[button_i].last_time = millis();                          // doesn't change is_measured to false, changes last time to current time -> resets the button -> function returns true every time the interval elapses
        }
        else{
          button[button_i].is_measured = false;                           // after the interval elapses, is_measured = false -> returns true only once
        }
        return true;
      }
    }
  }
  return false;
}

void control_led(int i, bool light_on){
digitalWrite(led[i].pin, light_on ? ON : OFF);
}

void change_led(int led_i){
  led[led_i].light_on ? control_led(led_i, false) : control_led(led_i, true);
}

// used in binary decomposition - swaps the leds -> MSb = LSb and LSb = MSb
int swap(int k){
  switch (k){
    case 0:
      return 3;
      break;
    case 1:
      return 2;
      break;
    case 2:
      return 1;
      break;
    case 3:
      return 0;
      break;
    default:
      return 0;
      break;
  }
}

// used in x3_3 and x3_4
// x3_4 has LS_b on the lowest led -> swap
void binary_decomposition(int n, bool is_swap = true){
  for (int k = 0; k < led_count; ++k){
    if (n & (1 << k)){
      is_swap ? control_led(swap(k), true) : control_led(k, true);
    }
  }
}

void light_off_leds(){
  for(int i = 0; i < led_count; ++i) control_led(i, false);
}

// used in x3_3 and x3_5 - updates leds to the current binary decomposition of counter.n
void update_led(){
  light_off_leds();
  binary_decomposition(counter.n); 
}

// used in x3_5 - does the required operation: + modulo, - modulo
void button_increment_decrement_operation(bool is_increment){
  is_increment ? counter.n = (counter.n + 1) % 16 : counter.n = (counter.n - 1) % 16;
  update_led();
}

// used in x3_5 - restarts the managment logic
void restart_managment(int button_i){
  button[button_i].first_access = true; 
  button[button_i].immidiate_access = false;
}

// very essential funtion for x3_5 
// controls the logic of each button independently
void button_increment_decrement(int button_i, int delay_interval, int count_interval, bool is_increment){
  if (button[button_i].first_access){                                 
    if (button_delay(button_i, 0)){                               // if button pressed for the first time -> does the operation and updates leds
      button_increment_decrement_operation(is_increment);
    }
    button[button_i].first_access = false;
  }

  if (button[button_i].immidiate_access || button_delay(button_i, delay_interval)){           // waited already delay_interval in previous interations or delay_interval has just elapsed
    if (!button[button_i].immidiate_access || button_delay(button_i, count_interval)){        // if just waited for the delay_interval -> does the operation and updates leds immidiately
      button_increment_decrement_operation(is_increment);                                     // if not, waits for the button_delay interval to elapse
    }
    button[button_i].immidiate_access = true;
  }
}

// Exercices functions
void x3_1_light_button(int led_i, int button_i){
  button_pressed(button_i) ? control_led(led_i, true) : control_led(led_i, false);
}

void x3_2_button_led_control(int led_i, int button_i){
  if(button_delay(button_i, 0, false)){
    change_led(led_i);
    led[led_i].light_on = !led[led_i].light_on;
  }
}

void x3_3_button_binary_count_up(){
  int zero_interval = 0;       // button_delay function needs to have specified interval for how long to measure - if set to 0 -> returns true immidiately when the button is pressed 
  if (button_delay(0, zero_interval, false)){
    counter.n += 1;
  }
  if (button_delay(1,zero_interval, false)){
    if (counter.n > 0) counter.n -= 1;
  }
  if (button_delay(2, zero_interval, false)){
    counter.n = 0;
  }
  update_led();
}

void x3_4_button_delay_flashing_led(int delay_interval, int flash_interval, int button_i, int led_i){
  if (led[led_i].is_flashing){
    if ((led_delay(led_i, flash_interval))){                      // flashes the led in the flash_interval
      control_led(led_i, led[led_i].light_on);
      led[led_i].light_on = !led[led_i].light_on;
    }

    if (button_delay(button_i, 0, false)) {                       // if the button pressed, stops flashing the led
      led[led_i].is_flashing = false;
      control_led(led_i, false);
    }

  }
  else{
    if (button_delay(button_i, delay_interval, false)){           // led is not flashing -> waits for the button to be pressed for delay_interval
      led[led_i].is_flashing = true;
    }
  }
}

void x3_5_button_binary_count_up_delayed(int delay_interval, int count_interval){
  if (button_pressed(2)){
    counter.n = 0;
    light_off_leds();
  }

  if (button_pressed(0)){
    button_increment_decrement(0, delay_interval, count_interval, true); // increment
  }
  else{
    restart_managment(0);
  }

  if (button_pressed(1)){
    button_increment_decrement(1, delay_interval, count_interval, false); // decrement 
  }
  else{
    restart_managment(1);
  }
}

void setup() {
  // initializes leds
  for (int i = 0; i < led_count; ++i){
    pinMode(led[i].pin, OUTPUT);
    digitalWrite(led[i].pin, HIGH);
  } 

  // initializes buttons
  for (int i = 0; i < button_count; ++i){
    pinMode(button[i].pin, INPUT);
  }
}

void loop() {
  //x3_1_light_button(1,0);      
  //x3_2_button_led_control(0,0);  
  //x3_3_button_binary_count_up();
  //x3_4_button_delay_flashing_led(1000, 300, 0, 0);      
  x3_5_button_binary_count_up_delayed(1000, 300); 
}
