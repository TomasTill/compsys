#include "funshield.h"

//setup promennych
class Button{
  public:
    int pin;
    unsigned long last_time = 0;
    bool last_state = false;                // used in button_delay - very essantial
    bool is_measured = false;               // used in button delay - very essantial

    Button(int pin_){
      pin = pin_;
    }
};

class Button_display_counter_managment_variables{
  public:
    int counter = 0;      
    int position = 0;     // position of the display

    Button_display_counter_managment_variables(int counter_, int starting_position_){
      counter = counter_; position = starting_position_;
    }
};
Button_display_counter_managment_variables m (0,0);     // m stands for managment, starting position is 0 -> in the beggining focus is on the rightest display

//button initialization
Button button1(button1_pin); Button button2 (button2_pin); Button button3 (button3_pin);
Button button[]{button1, button2, button3};
constexpr int button_count = sizeof(button)/sizeof(button[0]);

//generic functions
bool button_pressed(int button_i){
  return digitalRead(button[button_i].pin) ? false : true;
}

// very essential function. Used in x3_2, x3_3, x3_4
// Has two states 
// 1. returns true after the given interval elapsed even if the button was not released and it's still held down-> default state -> true
// 2. returns true after the given interval elapsed only once. Does not return true if the button was not released -> false 
bool button_delay(int button_i, int interval, bool measure_after_still_pressed = false){
  bool cur_state = button_pressed(button_i);
  if (cur_state != button[button_i].last_state){                          // has the state of the button changed?
    if (button_pressed(button_i)){
      button[button_i].last_time = millis();                              // if yes and the button is pressed, starts measuring from the current time
      button[button_i].is_measured = true;
    }
    else{
      button[button_i].is_measured = false;                               // if the state has changed but the button is not pressed -> from pressed to unpressed -> stops measuring
    }
    button[button_i].last_state = cur_state;                              // updates the state of the button
  }

  if (button[button_i].is_measured){                                      // button is measured only if it's pressed
    if (button_pressed(button_i)){
      if (millis() >= (button[button_i].last_time + interval)){
        if (measure_after_still_pressed){
          button[button_i].last_time = millis();                          // doesn't change is_measured to false, changes last time to current time -> resets the button -> function returns true every time the interval elapses
        }
        else{
          button[button_i].is_measured = false;                           // after the interval elapses, is_measured = false -> returns true only once
        }
        return true;
      }
    }
  }
  return false;
}

//specific functions for this exercise
void x4_2write_glyph_bitmask(int byte_glyph, int byte_pos_mask)
{
  digitalWrite(latch_pin, LOW);
  shiftOut(data_pin, clock_pin, MSBFIRST, byte_glyph);
  shiftOut(data_pin, clock_pin, MSBFIRST, byte_pos_mask);
  digitalWrite(latch_pin, HIGH);
}

int number_position(int position)
{
  switch (position){
    case 1: position = 8; break;
    case 2: position = 4; break;
    case 4: position = 2; break;
    case 8: position = 1; break; 
  }
  return position;
}

void x4_3write_number(int number, int position, bool show_dot = false){
  constexpr int glyph_number[] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90};
  constexpr int bitmask_position[] = {1 << 0, 1 << 1, 1 << 2, 1 << 3};                // 0th position is the rightest one

  digitalWrite(latch_pin, LOW);
  shiftOut(data_pin, clock_pin, MSBFIRST, glyph_number[number]);
  shiftOut(data_pin, clock_pin, MSBFIRST, number_position(bitmask_position[position]));
  digitalWrite(latch_pin, HIGH);
}


void update_display()
{
  x4_3write_number(m.counter, m.position);
}

void x4_4button_display_counter(){
  int zero_interval = 0;       // button_delay function needs to have specified interval for how long to measure - if set to 0 -> returns true immidiately when the button is pressed 
  if (button_delay(0, zero_interval)){
    m.counter = (m.counter + 1) % 10;
  }
  if (button_delay(1,zero_interval)){
    if (m.counter == 0) m.counter = 9;
    else m.counter -= 1;
  }

  if (button_delay(2, zero_interval)){
    m.position = (m.position + 1) % 4;
  }
  update_display();
}


int get_increment(int position){
  int increment;
  switch (position){
    case 0: increment = 1; break;
    case 1: increment = 10; break;
    case 2: increment = 100; break;
    case 3: increment = 1000; break;
  }
  return increment;
}

int get_show_number(int increment){
  int show_number = (m.counter / increment) % 10;
  return show_number;
}

void x4_5button_display_full_counter(){
  int zero_seconds = 0;    //button_delay function needs to have a specified interval for how long to measure - more details in the function specifics
  int first_button = 0; int second_button = 1; int third_button = 2;
  int increment = get_increment(m.position); int modulo = 10000;          // current increment
  int show_number = get_show_number(increment);                           // number to show on the display

  x4_3write_number(show_number, m.position);

  if (button_delay(first_button, zero_seconds)){
    m.counter = (m.counter + increment) % modulo;    // first button adds
  }
  if (button_delay(second_button, zero_seconds)){
    if ((m.counter - increment) < 0) m.counter = modulo - (increment - m.counter);
    else m.counter = (m.counter - increment);       // second button substracts

  }
  if (button_delay(third_button, zero_seconds)){
    m.position = (m.position + 1) % 4;              // there is four displays
  }


}

void setup() {
  // initializes buttonsjj
  for (int i = 0; i < button_count; ++i){
    pinMode(button[i].pin, INPUT);
  }

  //Serial.begin(9600);
  pinMode(latch_pin, OUTPUT);
  pinMode(data_pin, OUTPUT);
  pinMode(clock_pin, OUTPUT);
  x4_3write_number(0xFF,0x0F);
}

void loop() {
  //x4_2write_glyph_bitmask(0x30, 1 << 0);
  //x4_3write_number(m.counter,0);
  //x4_4button_display_counter();
  x4_5button_display_full_counter();
}