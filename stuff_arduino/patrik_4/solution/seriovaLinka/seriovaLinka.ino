#include "funshield.h"

constexpr int glyphDigit[11] = { 0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8, 0x80, 0x90, 0xff };

class Debug {
  public:
  Debug(bool isEnabled) : enabled(isEnabled) {};
  void out(const char* str)
  {
    if (enabled) {
      Serial.print("> ");
      Serial.print(str);
      Serial.print("\n");
    }
  }
  void varValue(const char* varName, const int value)
  {
    if (enabled) {
      Serial.print(varName);
      Serial.print(": ");
      Serial.print(value);
      Serial.print("\n");
    }
  }
  private:
  bool enabled;
};

class Button {
  public:
    Button(int buttonIndex) : _buttonIndex(buttonIndex) {};
    
    bool isButtonPressed() {
      long int currTime = millis();
//       small delay between presses
      if (currTime >= lastPressed + 100) {
        lastPressed = currTime;
        return !digitalRead(_buttonIndex);
      } else {
        return false;
      }
    }
    
    int getIndex() {
      return _buttonIndex;
    }
  private:
  int _buttonIndex;
  long int lastPressed = 0;
};

class SegmentDisplay {
  public:
  
  void writeGlyph(byte glyph, byte posBitMask) {
    digitalWrite(latch_pin, LOW);
    shiftOut(data_pin, clock_pin, MSBFIRST, glyph);
    shiftOut(data_pin, clock_pin, MSBFIRST, posBitMask);
    digitalWrite(latch_pin, HIGH);
  }
  
   void writeGlyphL(byte glyph, int posFromLeft) {
    writeGlyph(glyph, 1 << posFromLeft);
   }

   void writeGlyphR(byte glyph, int posFromRight) {
    writeGlyph(glyph, 0b1000 >> posFromRight);
   }

   void writeNumberL(int num, int posFromLeft) {
    writeGlyphL(glyphDigit[num], posFromLeft);
   }

   void writeNumberR(int num, int posFromRight) {
    writeGlyphR(glyphDigit[num], posFromRight);
   }
  
};
Button buttons[] = { Button(button1_pin), Button(button2_pin), Button(button3_pin) };
constexpr int buttonCount = sizeof(buttons) / sizeof(buttons[0]);
SegmentDisplay segDis = SegmentDisplay();
Debug d = Debug(true);

class Counter {
  public:
  void smallCounter() {
    if (buttons[0].isButtonPressed()) {
      ++counter;
      counter %= 10;
    } else if (buttons[1].isButtonPressed()) {
      --counter;
      if (counter == -1) {
        counter = 9;
      }
    } else if (buttons[2].isButtonPressed()) {
      counter = 0;
    }
    
    
    segDis.writeNumberL(counter, 1);
  }

  void bigCounter() {
    if (buttons[0].isButtonPressed()) {
      counter += diff;
      counter %= 10000;
      d.varValue("counter", counter);
    } else if (buttons[1].isButtonPressed()) {
      counter -= diff;
      if (counter < 0) {
        counter = 10000 + counter;
      }
      d.varValue("counter", counter);
    } else if (buttons[2].isButtonPressed()) {
      ++order;
      order %= 4;
      diff *= 10;
      if (diff > 1000) {
        diff = 1;
      }
      d.varValue("diff", diff);
      d.varValue("order", order);
    }
    int value = (counter / diff) % 10;
//    d.varValue("value", value);
    segDis.writeNumberR(value, order);
  }

  private:
  long int counter = 0;
  int order = 0; // zero indexed
  long int diff = 1;
};

Counter c;

void x41_glyphOnPos(byte glyph, int pos) {
  segDis.writeGlyphR(glyph, pos);
}

void x42_displayDigit(int num, int pos) {
  segDis.writeNumberR(num, pos);
}

void x43_bigCounter() {
  c.bigCounter();
}

void initButtons(){
  for (int i = 0; i < buttonCount; ++i){
    pinMode(buttons[i].getIndex(), INPUT);
  }
}
void setup() {
  Serial.begin(9600);
  initButtons();
  pinMode(latch_pin, OUTPUT);
  pinMode(clock_pin, OUTPUT);
  pinMode(data_pin, OUTPUT);
  segDis.writeGlyph(0xFF, 0xF);
}


void loop() {
//   x41_glyphOnPos(glyphDigit[2], 1);
//   x42_displayDigit(3, 3);
   x43_bigCounter();
}
